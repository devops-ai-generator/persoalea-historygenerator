import {Configuration, OpenAIApi} from "openai";
import NextCors from 'nextjs-cors';

const configuration = new Configuration({
    apiKey: process.env.OPENAI_API_KEY,
});
const openai = new OpenAIApi(configuration);

export default async function (req, res) {
    if (!configuration.apiKey) {
        res.status(500).json({
            error: {
                message: "OpenAI API key not configured, please follow instructions in README.md",
            }
        });
        return;
    }

    await NextCors(req, res, {
        // Options
        methods: ['GET', 'HEAD', 'PUT', 'PATCH', 'POST', 'DELETE'],
        origin: '*',
        optionsSuccessStatus: 200,
    });

    // const character = req.body.character || '';
    const characterRace = req.body.characterRace || '';
    const characterClass = req.body.characterClass || '';
    const characterComment = req.body.characterComment || '';
    const characterGenre = req.body.characterGenre || '';

    try {
        const completion = await openai.createCompletion({
            model: "text-davinci-003",
            prompt: generatePrompt(characterRace, characterClass, characterComment, characterGenre),
            temperature: 0.7,
            max_tokens: 500,
        });
        res.status(200).json({text: completion.data.choices[0].text});
    } catch (error) {
        // Consider adjusting the error handling logic for your use case
        if (error.response) {
            console.error(error.response.status, error.response.data);
            res.status(error.response.status).json(error.response.data);
        } else {
            console.error(`Error with OpenAI API request: ${error.message}`);
            res.status(500).json({
                error: {
                    message: 'An error occurred during your request.',
                }
            });
        }
    }
}

function generatePrompt(characterRace, characterClass, characterComment, characterGenre) {
    let prompt =
        "Ecris moi l'histoire d'un personnage et Invente lui un nom." +
        "Ce personnage est un " +
        characterRace +
        ", de genre " +
        characterGenre +
        " et c'est un " +
        characterClass +
        "Invente et décris ses traits de caractères, donne lui un objectif épique car ce personnage s'aprête à commencer une quète initiatique.";
    if (characterComment !== "") {
        prompt += "Egalement, " + characterComment;
    }
    return prompt;
}
